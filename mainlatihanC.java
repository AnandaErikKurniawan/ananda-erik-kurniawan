import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Masukkan nama Anda: ");
        String nama = input.nextLine();
        System.out.print("Masukkan program studi Anda: ");
        String programStudi = input.nextLine();
        System.out.print("Masukkan nilai Anda: ");
        double nilai = input.nextDouble();
        
        Mahasiswa mhs = new Mahasiswa(nama, programStudi, nilai);
        
        System.out.println("Nama: " + mhs.getNama());
        System.out.println("Program Studi: " + mhs.getProgramStudi());
        System.out.println("Nilai: " + mhs.getNilai());
        System.out.println("Nilai Huruf: " + mhs.getNilaiHuruf());
    }
}
