import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nama: ");
        String nama = input.nextLine();
        System.out.print("Masukkan nomor pelanggan: ");
        String noPelanggan = input.nextLine();
        System.out.print("Masukkan pemakaian air (dalam m3): ");
        int pemakaianAir = input.nextInt();

        Pelanggan pelanggan = new Pelanggan(nama, noPelanggan, pemakaianAir);

        System.out.println("\n=====Data Pelanggan=====");
        System.out.println("Nama Pelanggan: " + pelanggan.getNama());
        System.out.println("Nomor Pelanggan: " + pelanggan.getNoPelanggan());
        System.out.println("Pemakaian Air (m3): " + pelanggan.getPemakaianAir());
        System.out.println("Biaya Pemakaian Air: Rp " + pelanggan.getBiayaPakai());
    }
}
