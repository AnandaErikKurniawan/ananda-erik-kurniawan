public class Mahasiswa {
    private String nama;
    private String programStudi;
    private double nilai;
    private String nilaiHuruf;
    
    public Mahasiswa(String nama, String programStudi, double nilai) {
        this.nama = nama;
        this.programStudi = programStudi;
        this.nilai = nilai;
        
        if (nilai >= 90) {
            this.nilaiHuruf = "A";
        } else if (nilai >= 80) {
            this.nilaiHuruf = "B";
        } else if (nilai >= 70) {
            this.nilaiHuruf = "C";
        } else if (nilai >= 60) {
            this.nilaiHuruf = "D";
        } else (nilai >= 50) {
            this.nilaiHuruf = "E";
        }
    }
    
    public String getNama() {
        return nama;
    }
    
    public String getProgramStudi() {
        return programStudi;
    }
    
    public double getNilai() {
        return nilai;
    }
    
    public String getNilaiHuruf() {
        return nilaiHuruf;
    }
}
