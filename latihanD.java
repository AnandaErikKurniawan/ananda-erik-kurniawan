public class Pelanggan {
    private String nama;
    private String noPelanggan;
    private int pemakaianAir;
    private int biayaPakai;

    public Pelanggan(String nama, String noPelanggan, int pemakaianAir) {
        this.nama = nama;
        this.noPelanggan = noPelanggan;
        this.pemakaianAir = pemakaianAir;
        this.biayaPakai = hitungBiayaPakai(pemakaianAir);
    }

    public String getNama() {
        return nama;
    }

    public String getNoPelanggan() {
        return noPelanggan;
    }

    public int getPemakaianAir() {
        return pemakaianAir;
    }

    public int getBiayaPakai() {
        return biayaPakai;
    }

    private int hitungBiayaPakai(int pemakaianAir) {
        int biaya = 0;
        if (pemakaianAir <= 10) {
            biaya = pemakaianAir * 1000;
        } else if (pemakaianAir <= 20) {
            biaya = 10 * 1000 + (pemakaianAir - 10) * 2000;
        } else {
            biaya = 10 * 1000 + 10 * 2000 + (pemakaianAir - 20) * 5000;
        }
        return biaya;
    }
}
